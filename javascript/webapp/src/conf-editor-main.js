import Vue from "vue";
import AsyncComputed from "vue-async-computed";
import VueResource from "vue-resource";
import ConfigApp from "./ConfigApp.vue";
import axios from "axios";
import VueSimpleAlert from "vue-simple-alert";
import Spinner from "vue-simple-spinner";

Vue.config.productionTip = false;
Vue.use(AsyncComputed);
Vue.use(VueResource);
Vue.prototype.$axios = axios;
Vue.use(VueSimpleAlert);
Vue.component("vue-simple-spinner", Spinner);

new Vue({
  render: h => h(ConfigApp)
}).$mount("#app");
