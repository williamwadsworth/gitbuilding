import Vue from "vue";
import AsyncComputed from "vue-async-computed";
import VueResource from "vue-resource";
import ProjectApp from "./ProjectApp.vue";
import VueSimpleAlert from "vue-simple-alert";
import Spinner from "vue-simple-spinner";
import VueModal from "@kouts/vue-modal";
import "@kouts/vue-modal/dist/vue-modal.css";

Vue.config.productionTip = false;
Vue.use(AsyncComputed);
Vue.use(VueResource);
Vue.use(VueSimpleAlert);
Vue.component("vue-simple-spinner", Spinner);
Vue.component("Modal", VueModal);

new Vue({
  render: h => h(ProjectApp)
}).$mount("#app");
