import Vue from "vue";
import AsyncComputed from "vue-async-computed";
import VueResource from "vue-resource";
import BuildUpApp from "./BuildUpApp.vue";
import axios from "axios";
import VueSimpleAlert from "vue-simple-alert";
import Spinner from "vue-simple-spinner";
import VueModal from "@kouts/vue-modal";
import "@kouts/vue-modal/dist/vue-modal.css";
import VueSelect from "vue-select";

Vue.config.productionTip = false;
Vue.use(AsyncComputed);
Vue.use(VueResource);
Vue.prototype.$axios = axios;
Vue.use(VueSimpleAlert);
Vue.component("vue-simple-spinner", Spinner);
Vue.component("Modal", VueModal);
Vue.component("VueSelect", VueSelect);

new Vue({
  render: h => h(BuildUpApp)
}).$mount("#app");
