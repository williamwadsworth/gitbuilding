"""
This submodule constains the PageOrder class which is used to store and manipulate the paths
through the buildup documentation.
"""

import posixpath
from copy import copy, deepcopy
import logging

_LOGGER = logging.getLogger('BuildUp')

def _tree2list(tree, depth=0):
    """
    Input is a step tree for a page, output is a list detailing the order the pages
    should be displayed in. The list containts tuples, these tuples have three elements,
    the pagepath, the depth in the steptree, and the variables.
    """
    pagepath = None
    for key in tree.keys():
        if key.endswith(".md"):
            pagepath = key
    if pagepath is None:
        raise RuntimeError("Invalid tree list detected, no pagepath")
    variables = tree['variables']
    pagelist = [PageOrderEntry(pagepath, depth, variables)]
    for subtree in tree[pagepath]:
        pagelist += _tree2list(subtree, depth+1)
    return pagelist

class PageOrderEntry():
    """
    An entry for a page in PageOrder contains information such as the depth in a step tree and
    variables assigned to the page
    """
    def __init__(self, path, depth, variables):
        self.path = path
        self.depth = depth
        self.variables = variables
        self.bom_pages = None

    def set_bom_pages(self, bom_pages):
        """set any bill of material pages that are generated by this page entry"""
        self.bom_pages = bom_pages

    @property
    def md_bom_page(self):
        """Return the url of the markdown bill of materials page. Note that self.bom_pages
        contains all formats of the bill of materials (markdown, csv, and any formats to be
        added"""
        if self.bom_pages is None:
            return None
        #Markdown is the first entry in bom_pages
        return self.bom_pages[0]

    def generate_duplicate_entry(self, root):
        """
        Generate a duplicate entry for a pagelist with input root. This object
        is used in the PageOrder duplicates lists
        """
        return DuplicatePageEntry(self.path, self.depth, self.variables, root)

    def __eq__(self, other):
        return self.path == other

    def __repr__(self):
        return f"<PageOrderEntry path: {self.path}, variables: {self.variables}>"

class DuplicatePageEntry(PageOrderEntry):
    """A page order entry for a duplicate page"""

    def __init__(self, name, depth, variables, root):
        self.root = root
        super().__init__(name, depth, variables)

class PageOrder():
    """
    A PageOrder is object is initialised with the step trees from every page and the landing page.
    It will calculate the number of paths through the documentation, these can be accessed as
    trees or as lists.
    The lists have two versions, the list of the pages, and the list for navigation, the list for
    navigation also include bill of materials.
    """

    def __init__(self, trees, doc_obj):

        self._trees = deepcopy(trees)
        self._pagelists = [_tree2list(tree) for tree in self._trees]
        self._filenames_of_duplicates = []
        self._remove_empty_trees_and_lists()
        self._remove_non_rooted_trees()
        self._include_boms(doc_obj)
        if len(self.pagelists) > 0:
            self._validate_pagelists(doc_obj.landing_page)
        self._generate_masterlist_and_duplicates()
        self._calc_filenames_of_duplicates()

    @property
    def number_of_paths(self):
        """
        Read-only property which returns a the number of paths through the documentation
        """
        return len(self._pagelists)

    @property
    def trees(self):
        """
        Read-only property which returns a copy of the step trees. One for each path through
        the documentation.
        """
        return deepcopy(self._trees)

    @property
    def masterlist(self):
        """
        Read-only property which returns a list of all pages that are on any step list. The items
        in each list are just the page_names.
        """
        return copy(self._masterlist)

    @property
    def duplicates(self):
        """
        Read-only property which returns a dictionary. The keys are the pagepaths of any pages
        that are used in more than one path through the documentation, the value is a list of
        the pagepaths of the roots of those paths.
        """
        return deepcopy(self._duplicates)


    @property
    def filenames_of_duplicates(self):
        return copy(self._filenames_of_duplicates)

    def _calc_filenames_of_duplicates(self):
        filenames = []
        for duplicated_page in self.duplicates:
            for duplicate_entry in self.duplicates[duplicated_page]:
                root_pagepath = duplicate_entry.root
                list_no = self.get_list_number(root_pagepath)
                replacement_dict = self.link_replacement_dictionaries[list_no]
                filenames.append(replacement_dict[duplicated_page])
        self._filenames_of_duplicates = filenames

    @property
    def pagelists(self):
        """
        Read-only property which returns a copy of the pagelists. One list for each path through
        the documentation. The items in each list are a tuple of the page_names and their variables.
        """

        return deepcopy(self._pagelists)

    @property
    def link_replacement_dictionaries(self):
        """
        Read-only property which returns a copy of the pagelists. One list for each path through
        the documentation. The items in each list are a tuple of the file name in the input
        documentation and the file name in the output doucumentation.
        """
        out_lists = []
        for pagelist in self._pagelists:
            # first element in the lists is the first page, first element in the nav tuple
            # is the path
            rootpage_path = pagelist[0].path
            # subdirectory is the first page in the page order minus the '.md'
            subdir = rootpage_path[:-3]

            in_paths = []
            out_paths = []
            for page_entry in pagelist[1:]:
                if page_entry.path in self._duplicates:
                    in_paths.append(page_entry.path)
                    out_paths.append(posixpath.join(subdir, page_entry.path))
                    if page_entry.bom_pages is not None:
                        for bom_page in page_entry.bom_pages:
                            in_paths.append(bom_page)
                            out_paths.append(posixpath.join(subdir, bom_page))
            out_lists.append(dict(zip(in_paths, out_paths)))
        return out_lists

    def get_list_number(self, root_pagepath):
        """
        Return the number (index) of the pagelist with the input page name as root. If the input
        page is not a root of any pagelist then reutn None.
        """
        list_roots = [pagelist[0].path for pagelist in self.pagelists]
        try:
            return list_roots.index(root_pagepath)
        except ValueError:
            return None

    def _validate_pagelists(self, landing_page):
        """
        Checks the page order doesn't have unexpected problems like repeated steps. The
        landing page doesn't need to be in the page order list, but if it is in it must
        be at the start.
        The function does not return anything, it just logs any issues
        """
        for pagelist in self.pagelists:
            if len(pagelist) == 0:
                return

            if landing_page in pagelist[1:]:
                _LOGGER.warning('The landing page cannot be a step of another page.')

            #warn if steps are repeated
            pagepaths = [page_entry.path for page_entry in pagelist]
            if len(set(pagepaths)) != len(pagelist):
                _LOGGER.warning('The defined page order has the same step repeated: "%s"',
                                '->'.join(pagelist))

    def _remove_empty_trees_and_lists(self):
        """
        Some page lists contain only the name of the page the tree was read from as
        they have no steps. This function removes those lists and trees.
        """
        poplist = []
        for n, pagelist in enumerate(self._pagelists):
            if len(pagelist) == 1:
                poplist.append(n)
        #pop in reverse
        for n in poplist[::-1]:
            self._pagelists.pop(n)
            self._trees.pop(n)

    def _remove_non_rooted_trees(self):
        """
        This function removes any trees (and corresponding pagelists) that are not the
        root of a step tree.
        """
        #list of pagelists to remove as they are a sub-part of another list
        poplist = []
        for n, pagelist in enumerate(self._pagelists):
            for otherlist in self._pagelists[0:n]+self._pagelists[n+1:]:
                otherlist_names = [page_entry.path for page_entry in otherlist]
                if pagelist[0].path in otherlist_names:
                    poplist.append(n)
                    break
        #pop in reverse
        for n in poplist[::-1]:
            self._pagelists.pop(n)
            self._trees.pop(n)

    def _include_boms(self, doc_obj):
        """
        Add bills of materials to the pagelists tuple.
        """
        for pagelist in self._pagelists:
            for page_entry in pagelist:
                page = doc_obj.get_page_by_path(page_entry.path)
                page_entry.set_bom_pages(page.get_bom_page())

    def _generate_masterlist_and_duplicates(self):
        """
        Generate a list of all pages that are in step trees
        """
        self._masterlist = []
        reused_pages = []
        for pagelist in self.pagelists:
            for page_entry in pagelist:
                pagepath = page_entry.path
                if pagepath not in self._masterlist:
                    self._masterlist.append(pagepath)
                elif pagepath not in reused_pages:
                    reused_pages.append(page_entry)

        self._duplicates = {}
        for page_entry in reused_pages:
            pagepath = page_entry.path
            self._duplicates[pagepath] = []
            for pagelist in self.pagelists:
                if pagepath in pagelist:
                    root = pagelist[0].path
                    duplicate_entry = page_entry.generate_duplicate_entry(root)
                    self._duplicates[pagepath].append(duplicate_entry)

    def get_pagelist_for_page(self, page):
        """
        return a pagelist rooted at the specific page, and also returns
        the replacement links to go to the correct version of duplicated pages.
        """
        if page not in self.masterlist:
            raise IndexError(f"{page} is not in any page order")

        page_ind = None
        replace_links = None
        this_pagelist = None
        for n, pagelist in enumerate(self.pagelists):
            if page in pagelist:
                page_ind = pagelist.index(page)
                this_pagelist = pagelist
                if self.number_of_paths == 0:
                    replace_links = None
                else:
                    replace_links = self.link_replacement_dictionaries[n]
                break
        if (page_ind is None) or (this_pagelist is None):
            raise RuntimeError("Navigation page not found when in masterlist")

        sub_pageslist = [this_pagelist[page_ind]]
        page_level = sub_pageslist[0].depth
        ind = page_ind + 1
        while ind < len(this_pagelist) and this_pagelist[ind].depth > page_level:
            sub_pageslist.append(this_pagelist[ind])
            ind+=1
        return sub_pageslist, replace_links
