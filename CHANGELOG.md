# Changelog

## v0.12.2

#### Performance

* Significant performance boost when rebuilding large projects by caching duplicate file names of duplicated pages rather than recalculating.

## v0.12.1

#### Bug fixes

* Build-up data (such as part info) should allow double quotes to define a string, only single quotes worked as expected. Now fixed
* Very weird bug when a page is a duplicate, and has a part that has that has a note that has a link in it. This made the part link go to the wrong place. Now fixed, overlapping regex matches was the cause.
* Links to the missing page were not relative causing 404 errors

## v0.12.0

#### Core functionality

* Page tagging functionality added
* Category and notes are added to the CSV bill of materials
* Toolbar added to live editor with options such as Bold, Italic, Hyperlinks, part-links
* Significant improvement of how external files are handled, including option to specify external directories.
* Dropped Python 3.6 support

### Style

* Bill of Materials link has icons for HTML and CSV rather than having CSV in brackets as this led to confusion.
* HTML id added to each Bill of Material category
* Maths rendered using mathml rather than KaTeX (via latex2mathml package)

#### Bug fixes

* Missing part numbers for a part in a part library give validation error rather than cause a crash
* De-duplication of files when zipping
* Explicit version of python-markdown set to mitigate rendering issues
* Reference style links in build-up snippets throw a warning rather than fail silently
* Undo history no longer lost when dragging and dropping images
* Orphaned files no longer raise extra warnings about missing files
* Fixed links in default README for new projects
* License Text no longer is highlighted as code
* Reference style FromStep links resolve properly
* Instability and slow rendering caused by Markdown KaTeX fixed by switch to using mathml

## v0.11.0

#### Core functionality

* Implemented basic templating with include statements
* Added a way to pass variables to a step link, these can influence includes or be used directly in the page
* Part names are now case insensitive
* Output links can be hidden
* Added syntax for information links (i.e. links to extra information) which display as an info icon.
* Added special blockquotes for Caution, Warning, Help, and Information
* Can add details header to the page using the page YAML
* External links open in new tab

### Style

* Added an HTML div around Bill of Materials in a page. This can be customised, default style is a border.

#### Bug fixes

* Fix error message when unable to read part in YAML library
* Improved calculating where to link to during live editing of duplicate pages


## v0.10.1

#### Bug fixes

* Fix bug with navigation directing to pages without translated urls (wrong file extension)

## v0.10.0

#### Core functionality

* Editor warns if the page being edited is used for multiple instructions
* Implemented zip links, these zip up selected files into an archive and link to the archive
* Colour of STL preview is customisable
* `gitbuilding clean` will remove automatically created content
* Syntax highlighting of fenced code enabled.
* All markdown headers now create HTML ids for internal linking

#### Bug fixes

* Links within a page no longer throw "Linked file does not exist" warning

## v0.9.1

#### Bug fixes

* Unable to import into Python 3.6
* Link to CSV on Bill of materials page was incorrect if generating page was not in root directory
* Link to internal content on a page created file not found warnings
* Link to a "duplicated" page on a particular path through the documentation created warning
* 3D viewer loaded on every page even when not needed

#### Dev-changes

* Added dev requirements to setup.py
* Testing python 3.6, 3.7, 3.8 and 3.9 in CI

## v0.9.0

#### Core functionality

* YAML header can be added at the top of a build up page, currently used only for part data
* Custom navigation can autocomplete subnavigation from steps
* Warnings added for links to missing pages and files
* Katex added to render LaTeX
* More useful information added to CSV bill of materials
* Note text is separated the item in the Bill of materials
* Bill of materials is alphabetically ordered within sections
* Multiple parts can be listed per supplier in part libraries
* `--hardwarnings` and `--promotefussy` command line options added to change exit status if warnings are found
* GitLab CI script generator gzips files as default to improve performance

#### Internal changes
* YAML part data now parsed with marshmallow
* Bumped versions of many javascript packages

### Style

* Table style has been revamped to look cleaner
* Intrusive save pop-up removed

#### Bug fixes

* Fixed issue where edit button on a defined landing page created `index.md` rather than editing the correct page.
* Markdown links in a part's "Note" field is now processed correctly

## v0.8.4

#### Bug fixes
* Fix CSS on live server pages such as "new", "warnings", and "contents". Broken by previous patch release.

## v0.8.3

#### Minor fixes
* Clarify text on "missing" page and "multiple versions" page

#### Bug fixes
* Fix links and CSS on "missing" page

## v0.8.2

#### Bug fixes
* Fix unspecified character set in HTML template

## v0.8.1

#### Bug fixes
* Fix bug processing custom png favicons

## v0.8.0

#### Core functionality

* A global warning page shows all documentation warnings
* Interface added for creating a new page
* Moved from old STL viewer to our own GB3D viewer which views STL, WRL and GLB/glTF files. For GLB/glTF files can be used to show exploded views.
* PDF viewer capability
* Drag and drop for more file types (3D models, and PDF)
* Cleaner navigation - only shows the nested navigation for active sections

#### Syntax

* Introduced image style syntax for previewing files and YouTube videos. This replaces the `ad-hoc` replacement of links to STL files with a live preview.
* Ability to append `{previewpage}` to a link to a 3D model to link to a preview page. (Not available for part links, but STL still automatically links to a preview)
* The `ForceOutput` configuration option new accepts wildcards


#### Internal changes
* The live editor server uses waitress rather than the Flask dev server
* Clean up of the live server code
* Totally rewrote internal path/url handling - In the long term this should make GitBuilding more stable on Windows

#### Bug fixes
* Fixed strange issue with images showing on Windows despite having the wrong path
* Fixes to GitHub Action generator

## v0.7.1

#### Core
* Option `--no-server` added to `build-html` command. This appends `.html` to all links for offline use of HTML docs.
* Variable `Landing` now available for HTML templates, selectively appends `index.md` to root when in no-server mode

#### Bug fixes
* Bug where STLs and license page links were not changed to relative links for HTML
* Broken image in footer in live editor
* Fixed PDF page ordering when no steps are defined
* Fixed - Image could not be a link in markdown
* Fixed regex bug which caused crashes

## v0.7.0

#### Core functionality

* Multiple paths through the documentation now works. For example, two similar devices can share most of the same instruction steps.
* PDF export - (Experimental and somewhat hard to use on windows)
* CI generation - Generate basic CI scripts to create a documentation website via GitLab CI, Netlify or GitHub Actions.
* Bill of materials pages also generate a CSV file. Minimal implementation needs improving by putting more information into the CSV
* Bill of materials page now is shown in the page Navigation
* HTML output now has "Next" and "Previous" links to improve Navigation

#### Syntax

* Syntax to define a part created on one page, this can then be used later in the documentation
* Syntax to create a a bill of materials page (and link to it) changed to improve consistency. (Old syntax works but creates a depreciation warning.)
* Categories and Notes can be set by inline part links.

#### Bug fixes

* More windows filepath issues on the web server fixed (backported as v0.6.1)

## v0.6.0


#### Syntax

* Image galleries can have images on sequential lines.

#### Unit system

* A new unit system allows adding and comparison of mass, length, area, and volumes - selection is still limited

#### Server/Live editor

* Navigation fixed when no steps
* Added default image when path is broken
* Added method to create landing page if none exists
* Added basic contents page that only shows locally

#### Configuration options

* `HTMLOptions` - removed from `BuildConf.yaml` HTML customisation now done with Jinja templates.

#### HTML Customisation

* Jinja templates in `_templates` directory used for customising HTML.

#### Bug fixes

A number of bug fixes including

* Live editor links directing to wrong location for page in a sub-directory
* Dropped files not working for page in a sub-directory
* Cursor moving to end of file when image dropped
* If multiple images were dropped only one was added
* Live STL preview for links to an STL on its own line was broken
* Links with anchors now translate properly

## v0.0.1 - v0.5.0

No changelog maintained for these versions.
