## Developer installation

If you are interested in contributing to GitBuilding, or just running the most recent version from GitLab you will need to do a few things to make sure that the python and javascript are in sync.

For more information on contributing to the code, including how we run tests, and code-style see the [contributing guidelines](CONTRIBUTING.md).

#### To install as a developer

* Clone the repository with `git clone`
* Install as an editable package (see below)
* Build the javascript bundle (see below)

#### To update a developer install
* `git clone` Use GitClone to update the package
* Build the javascript bundle - if Javascript is updated (see below)

#### Common issues
If you have a developer submitting an issue, please check below:

* Is the Javascript is out of date? -- Are you sure you have run the javascript build recently?
* Python version number or requirements are out of date. -- Editable pip packages never update version number or requirements. Run `pip uninstall gitbuilding` and then install again as an editable package (see below)

## Installing with PIP as editable package

After you have cloned GitBuilding into a directory navigate to that directory and run:

```
pip install -e .
```

The `-e` will make sure that the edits to the python code are available straight away to python.


## Building the Gitbuilding Javascript bundle

The javascript for the gitbuilding server is built for the flask server. Right now we have two parts, the STL viewer and our live editor. The STL viewer is [GB3D](https://gitlab.com/gitbuilding/gb3d). The live viewer is a bit more complicated using Vue.js and a some other bits and bobs. The js built for distribution is included in the pip distributions, but is not pushed to GitLab.

### Requirements

To bundle the javascript you will need
* NodeJS >=8
* NPM >=5

For Window and Mac the best place to download this is the [Node download page](https://nodejs.org/en/download/), for Linux use the [NodeSource Node.js Binary Distributions](https://github.com/nodesource/distributions/blob/master/README.md#debinstall).

### Building the js

On Linux or Mac (or Windows with a bash terminal) you can run

```
./build_js.sh
```

If you are on Windows and don't have bash you will need to navigate into the `javascript/3d-viewer` directory and run:

```
npm install
npm run lint
npm run build
```
then repeat these commands in the `javascript/webapp` directory

This will output files to: gitbuilding/static

### Running the dev server

If you are making significant changes to the webapp it is probably worth running

```
npm run serve
```

in the webapp directory. You can then connect this page to gitbuilding with by adding a `--dev` flag to `serve`:

```
gitbuilding serve --dev
```


## Building wheels PyPi or other package repositories

To package GitBuilding up as a wheel you can run

```
./build_dist.sh
```
this will bundle the javascript and the build the distribution
