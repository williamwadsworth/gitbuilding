#! /usr/bin/env python

"""
A rather hacking script to made some dev documentation. Could probably do
something more clever with sphinx
"""

import os
import inspect
import importlib
from textwrap import dedent

def main():
    """
    Just loop through all python files in the main directory and
    output some docstrings.
    """
    all_modules = []
    for root, _, files in os.walk('../gitbuilding'):
        for filename in files:
            if filename.endswith('.py') and filename != '__init__.py':
                fullname = os.path.join(root, filename)
                fullname = os.path.relpath(fullname, '..')
                module_name = fullname[:-3].replace('/', '.')
                all_modules.append(module_name)

    output = '# GitBuilding DevDocs\n\n'
    output += '''This is auto generated with `dev_docs.py` it is not complete
documentation for development. It is just a way to visualise the code structure.

## Module import graph
![](gitbuilding.svg)

## Modules

'''
    for module_name in all_modules:
        output += f'\n\n### Sub-Module: {module_name}\n\n'
        module = importlib.import_module(module_name)

        if module.__doc__ is None:
            output += '> No documentation!'
        else:
            output += '> '+dedent(module.__doc__).replace('\n', ' ')
        class_members = inspect.getmembers(module, inspect.isclass)
        for class_member in class_members:
            class_name = class_member[0]
            the_class = class_member[1]
            if the_class.__module__ == module_name:
                output += f'\n\n#### Class: {module_name}.{class_name}\n\n'
                if the_class.__doc__ is None:
                    output += '> No documentation!'
                else:
                    output += '> '+dedent(the_class.__doc__).replace('\n', ' ')
    with open('README.md', 'w', encoding='utf-8') as outfile:
        outfile.write(output)

if __name__ == '__main__':
    main()
